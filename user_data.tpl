#!/bin/bash
echo ECS_CLUSTER=tf-${StackName}-${zone}-ecs > /etc/ecs/ecs.config
echo ECS_ENGINE_AUTH_TYPE=dockercfg >> /etc/ecs/ecs.config
echo ECS_ENGINE_AUTH_DATA={\"https://index.docker.io/v1/\": {\"auth\": \"${dockerhub_auth}\", \"email\": \"${dockerhub_email}\"}} >> /etc/ecs/ecs.config
echo ECS_AVAILABLE_LOGGING_DRIVERS=[\"json-file\",\"syslog\",\"gelf\"] >> /etc/ecs/ecs.config

yum install -y aws-cfn-bootstrap wget aws-cli jq

################## User defined UserData ####################
${CustomerUserData}
