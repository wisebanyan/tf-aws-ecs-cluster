
/**
 * Launch configuration used by autoscaling group
 */
resource "aws_launch_configuration" "ecs-lc" {
  # Using name_prefix will allow the LC to be updated and a unique suffix added
  name_prefix          = "tf-${var.stackname}-${var.zone}-ecs-lc-"
  image_id             = "${var.ami_id}"
  instance_type        = "${var.instance_type}"
  key_name             = "${var.key_name}"
  security_groups      = ["${aws_security_group.ecs.id}","${var.vpc_security_group}"]
  iam_instance_profile = "${var.iam_instance_role}"
  associate_public_ip_address = "${var.assign_public_ip}"
  user_data            = "${template_file.userdata.rendered}"

  # Adding lifecycle so that any changes to userdata or the launchconfiguration causes a new config to be created
  # and then applied. This prevents the issue when trying to update an LC with new settings which isn't supported
  # by AWS/
  lifecycle {
    create_before_destroy = true
  }
  depends_on = ["aws_ecs_cluster.ecs"]
}

resource "template_file" "userdata" {
  template = "${file("${path.module}/user_data.tpl")}"

  vars = {
    StackName= "${var.stackname}"
    zone = "${var.zone}"
    dockerhub_auth = "${var.dockerhub_auth_string}"
    dockerhub_email = "${var.dockerhub_email}"
    CONSUL_HOST = "${var.CONSUL_HOST}"
    DATADOG_API_KEY = "${var.DATADOG_API_KEY}"
    SaltMaster = "${var.saltmaster}"
    InternalHostedZone = "${var.InternalHostedZone}"
    CustomerUserData = "${var.userdata}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

/**
 * Autoscaling group.
 */
resource "aws_autoscaling_group" "ecs-asg" {
  vpc_zone_identifier = ["${split(",", var.aws_subnets)}"]
  name = "tf-${var.stackname}-${var.zone}-ecs-asg"
  max_size = "${var.asg_max}"
  min_size = "${var.asg_min}"
  desired_capacity = "${var.asg_desired}"
  force_delete = true
  launch_configuration = "${aws_launch_configuration.ecs-lc.name}"
  
  lifecycle {
    create_before_destroy = true
  }
  
  tag {
    key = "Name"
    value = "tf-${var.stackname}-${var.zone}-ecs-asg"
    propagate_at_launch = "true"
  }
  depends_on = ["aws_launch_configuration.ecs-lc"]
  health_check_type = "EC2"
}

/* ecs service cluster */
resource "aws_ecs_cluster" "ecs" {
  name = "tf-${var.stackname}-${var.zone}-ecs"
}
