output "ecs_cluster_id" {
  value = "${aws_ecs_cluster.ecs.id}"
}

output "ecs_cluster_name" {
  value = "${aws_ecs_cluster.ecs.name}"
}

output "ecs_cluster_lc" {
  value = "${aws_launch_configuration.ecs-lc.id}"
}

output "ecs_cluster_asg" {
  value = "${aws_autoscaling_group.ecs-asg.id}"
}

output "ecs_cluster_sg" {
  value = "${aws_security_group.ecs.id}"
}

output "ecs_asg_desired_capacity" {
  value = "${aws_autoscaling_group.ecs-asg.desired_capacity}"
}
