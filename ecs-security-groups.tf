/**
 * Provides internal access to container ports
 */
resource "aws_security_group" "ecs" {
  name = "tf-${var.stackname}-${var.zone}-ecs-sg"
  description = "Container Instance Allowed Ports"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port = 1
    to_port   = 65535
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 1
    to_port   = 65535
    protocol  = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "tf-${var.stackname}-${var.zone}-ecs-sg"
  }

  lifecycle {
    create_before_destroy = true
  }
}